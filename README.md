# WIP: CI example

This is an example of how to use [Database Lab](https://gitlab.com/postgres-ai/database-lab)
with GitLab CI to test migrations against production-sized database.

## Prerequirements
- Setup Database Lab Server. [Here](https://postgres.ai/docs/database-lab/1_tutorial) you can
read more about Database Lab Server setup.
- Set `DBLAB_INSTANCE_URL`, `DBLAB_VERIFICATION_TOKEN`, `POSTGRES_PASSWORD`
in `Repository settings / CI/CD / Variables`. Make them `Protected` and `Masked` (may be unavailable
for `DBLAB_INSTANCE_URL`).

## TODO
- Actually use sqitch (instead of psql)
- Add ability to use env variable to set password for `clone create` CLI command
- Build binary for alpine or Database Lab CLI Docker image (include jq)
- We have to usses artifacts to pass env variables from one stage to another, is there a better way?
- Security
    - Postgres password variable: generation on each build
    - Artifacts to pass env variables are public? How can we protect them?
    - Postgres instance are public (use custom runner in the same network, SSL, what else)
- Delete `clone.json` and `dblab` binary when those file will not be needed for testing purposes
- The older snapshot (of production DB) the more migrations have to be deployed upon the one that being tested
